import React, { Component } from 'react';
class Pentagon extends React.Component {
    constructor(props){
        super(props);
    } 
    render() {
      return (
        <div className="pentagon" dangerouslySetInnerHTML={{__html: '<svg stroke="'+this.props.options.hover+'" stroke-width="5" width="'+this.props.options.width+'" height="'+this.props.options.height+'" fill="'+this.props.options.color+'" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 301 285"> <metadata><?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?><x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01 "> <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"> <rdf:Description rdf:about=""/> </rdf:RDF></x:xmpmeta> <?xpacket end="w"?></metadata><defs> <style>.cls-1{fill-rule: evenodd;}</style> </defs> <path id="Polygon_1" data-name="Polygon 1" class="cls-1" d="M57,2L241.786-.335l59.323,175.019-148.122,110.5L2.119,178.463Z"/></svg>'}} />
      );
    }
}
export default Pentagon