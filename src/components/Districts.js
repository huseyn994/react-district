import React from 'react';
import DistrictApi from '../api'; // Fake API of districts.
import District from './District'; // District component.
 
// Taking attention of discrits => if there is not action, districts will shake to take attention.
setInterval(function(){
    let l = document.getElementsByClassName("district").length
    for(let i=0;i<l;i++){
        document.getElementsByClassName("district")[i].classList.toggle("shake")
    }
},60000); // every one minute.

const Districts = () => {
    return (
        <div className="districts">
            {/*Fetching districts from API*/}
            <div className="row">
                {DistrictApi.all().map((item, index) => (
                    <District key={index} index={index} options={item} />
                ))}
            </div>    
        </div>
    );
}
export default Districts;