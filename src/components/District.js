import React from 'react';
import { Link } from 'react-router-dom'
import DistrictApi from '../api' // Fake API of districts.
import Pentagon from './pentagon'; // I made pentagon shape as component due to manipulation reasons of SVG.

const renderPentagon = (options)=>{
    return <Pentagon options={options}/>
}
const District = (props)=>{
    return (
        <div className="district">
            {/*Generating district*/}            
            <div className="district_name">
                    <Link to={`/district/${props.options.id}`}>
                        <h3>
                            <span className="district_id">{props.options.id}.</span>
                            {props.options.name}
                        </h3>
                    </Link>
            </div>
            {/*Every district pentagon has own color, so I put these data to API*/} 
            {renderPentagon((DistrictApi.getStyle(props.options.id)))}
        </div>        
    );
}
export default District