import React from 'react';
import { Link } from 'react-router-dom'
import DistrictApi from '../api'
import Districts from './Districts'
import NotFound from './404';
const renderModal=()=>{
  
}
const PopUp = (props) => {
  const district = DistrictApi.get(
    parseInt(props.match.params.id, 10)
  )
  // deliberetly delaying of data to show hamburger loader.
  setTimeout(function() {
    document.getElementById('hamburgerLoader').style.display = 'none';
    document.getElementsByClassName('modal-dialog')[0].style.display = 'block';
  }, 1000);


  if (!district) {
    return <NotFound/>
  }
  return (

    <div className="modal">
        <div id="hamburgerLoader"></div>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Delivering to {district.name} street</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><Link to={`/`}>&times;</Link></span>
              </button>
            </div>
            <div className="modal-body">
            {district.delivery ? (
              <p>We delivery our product to {district.name} street {district.delivery.delivery_price} forints within {district.delivery.delivery_time} minutes
              </p>
            ) : (
              <p>Sadly we do not delivery to this district yet, please come back later</p>
            )}              
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal"><Link to={`/`}>Close</Link></button>
            </div>
          </div>
        </div>
    </div> 
  )
}

export default PopUp;