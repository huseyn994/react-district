import React from 'react';
import { Switch, Route } from 'react-router-dom'
import Districts from './Districts'; // Districts page
import PopUp from './PopUp'; // PopUp page
import NotFound from './404'; // 404 Not found
const Map = () => {
    return (
      <div className="wrapper">
        <div className="container-fluid">
          {/*Content chance occurs in this Switch*/}
          <Switch>
            <Route exact path='/' component={Districts}/>
            <Route path='/district/:id' component={PopUp}/>
            <Route path="*" component={NotFound}/>{/* Otherwise this route will be triggered */}
          </Switch>  
        </div>
      </div>
    )
}; 
export default Map;