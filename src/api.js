// Fake District API.
const DistrictApi = {
    districts: [
        {
            'id': 10,
            'name':'Hajnal',
            'delivery':{
                'delivery_price': 5000,
                'delivery_time': 25
             },
             'pentegon_style':{'color':'#db1b22', hover:'#710509', width:250, height:250}
        },
        {    
            'id': 4,
            'name':'Kassai',
            'delivery':{
                'delivery_price': 7000,
                'delivery_time': 10
            },
            'pentegon_style':{color:'#1b29db', hover:'#010867', width:250, height:250}
        },
        // This object even does not has delivery object, it means,
        // this object can be procced in delivery process.
        { 
            'id': 2,
            'name':'Bajy zlinksy',
            'pentegon_style':{color:'#fcff00', hover:'#9fa002', width:250, height:250}
        }
    ],
    
    all: function() { return this.districts},
    get: function(id) {
      const isDistrict = p => p.id === id
      return this.districts.find(isDistrict)
    },
    getStyle:function(id){
        return this.get(id)['pentegon_style']
    }
  }
  
  export default DistrictApi